#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import, unicode_literals

import os.path

def changeext(filename, ext):
    if ext[0] != '.':
        ext = '.' + ext
    root, _= os.path.splitext(filename)
    return root + ext

def test():
    assert 'filename.ext' == changeext('filename.bin', '.ext')
    assert 'filename.ext' == changeext('filename.bin', 'ext')
    assert 'filename.ext' == changeext('filename', 'ext')
    
if __name__ == '__main__':
    test()
