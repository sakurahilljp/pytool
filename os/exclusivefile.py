#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import, unicode_literals
import json
import os.path
import threading

try:
    import fcntl
except:
    import dummyfcntl as fcntl


class ExclusiveFile(object):

    def __init__(self, filename, mode='r'):
        self.filename = filename
        self.mode = mode
        self.fp = None

    def __enter__(self):
        self.acquire()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.release()

    def __del__(self):
        self.release()

    def read(self, size=-1):
        return self.fp.read(size)

    def readline(self, size=-1):
        return self.fp.readline(size)

    def write(self, s):
        self.fp.write(s)

    @property
    def locked(self):
        return self.fp != None

    def acquire(self):
        assert self.fp == None

        self.fp = open(self.filename, self.mode)
        fcntl.flock(self.fp, fcntl.LOCK_EX)

    def release(self):
        if self.fp is None:
            return

        try:
            fcntl.flock(self.fp, fcntl.LOCK_UN)
            self.fp.close()
        except:
            pass
        finally:
            self.fp = None


class JsonExclusiveFile(ExclusiveFile):

    def __init__(self, filename, mode='r'):
        super(JsonExclusiveFile, self).__init__(filename, mode)

    def dump(self, obj):
        self.fp.seek(0)
        json.dump(obj, self.fp)
        self.fp.truncate()

    def load(self):
        self.fp.seek(0)
        return json.load(self.fp)


class ExclusiveDictStorage(dict):

    def __init__(self, filename):
        super(dict, self).__init__()

        if not os.path.exists(filename):
            with open(filename, 'w'):
                pass

        self.fp = JsonExclusiveFile(filename, 'r+')

    def __enter__(self):
        self.acquire()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.release()

    def __del__(self):
        self.release()

    def acquire(self):
        if self.fp.locked:
            return

        self.clear()
        self.fp.acquire()

        try:
            stored = self.fp.load()
        except ValueError:
            # ValueError("No JSON object could be decoded")
            pass

        if isinstance(stored, dict):
            self.update(stored)

    def release(self):
        if self.fp.locked:
            self.fp.dump(self)
            self.fp.release()


def main():
    import time
    with ExclusiveFile('rmok.dat', 'w') as fp:
        print('acquire lock')
        fp.write('1')
        time.sleep(5)
    print('release lock')

    with JsonExclusiveFile('json.dat', 'w') as f:
        f.dump({'hoge': 1, 'fuga': 2})

    with JsonExclusiveFile('json.dat', 'r') as f:
        print(f.load())

    storage = ExclusiveDictStorage('storage.dat')

    with storage:
        storage['key1'] = 1

    with storage:
        storage['key2'] = 2

    with storage:
        print(storage['key1'], storage['key2'])

        for k in storage.iterkeys():
            print('k -->', k)

        for v in storage.itervalues():
            print('v -->', v)

        for k, v in storage.iteritems():
            print('k,v -->', k, v)

        if 'key1' in storage:
            print('key1 is contained.')

if __name__ == "__main__":
    main()
