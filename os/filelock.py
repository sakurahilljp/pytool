#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import, unicode_literals

import os
import os.path
import tempfile
import time
import errno
from contextlib import contextmanager

try:
    import fcntl
except:
    import dummyfcntl as fcntl

POLLING_SPAN = 0.5


@contextmanager
def locking(lockfile, timeout=None):

    path = (lockfile if os.path.isabs(lockfile)
            else os.path.join(tempfile.gettempdir(), lockfile))
    lock = FileLock(path)

    if lock.acquire(timeout):
        try:
            yield
        finally:
            lock.release()


class FileLock(object):

    def __init__(self, lockfile):
        self.lockfile = lockfile
        self.fd = None

    def acquire(self, timeout=None):
        # timeout (in second):
        # None        --> wait forever,
        # 0 or larger --> wait for specified seconds

        self.fd = os.open(self.lockfile, os.O_CREAT)

        if timeout == None:
            # wait forever
            fcntl.flock(self.fd, fcntl.LOCK_EX)
            return True

        expired = time.time() + timeout
        while time.time() <= expired:
            try:
                fcntl.flock(self.fd, fcntl.LOCK_EX | fcntl.LOCK_NB)
                return True
            except IOError as ex:
                if ex.errno not in (errno.EAGAIN, errno.EACCES):
                    raise
            time.sleep(POLLING_SPAN)

        return False

    def release(self):
        if self.fd is None:
            return

        fcntl.flock(self.fd, fcntl.LOCK_UN)
        os.close(self.fd)
        self.fd = None

        # Try to remove the lock file, but don't try too hard because it is
        # unnecessary. This is mostly to help the user see whether a lock
        # exists by examining the filesystem.
        try:
            os.unlink(self.lockfile)
        except:
            pass

    def __del__(self):
        self.release()


def main():
    time.sleep(3)
    with locking('filelock.lock'):
        print('>>> enter lock (PID:{})'.format(os.getpid()))
        time.sleep(8)
        print('<<< exit lock (PID:{})'.format(os.getpid()))

if __name__ == "__main__":
    main()
