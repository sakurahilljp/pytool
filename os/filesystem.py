#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import, unicode_literals

import sys
import os
import fnmatch

def iterfiles(dirpath, predicate=lambda x: True):
    for root, dirs, files in os.walk(dirpath):
        for fname in files:
            fpath = os.path.join(root, fname)
            if predicate(fpath):
                yield fpath

def walk(root, patterns='*', single_level=False, yield_folders=False):
    patterns = patterns.split(';')

    for path, subdirs, files in os.walk(root):
        if yield_folders:
            files.extend(subdirs)
        files.sort()

        for name in files:
            for pattern in patterns:
                if fnmatch.fnmatch(name, pattern):
                    yield os.path.join(path,name)
                    break
        if single_level:
            break


def test(path):
    print('*** iterfiles without predicate')
    for a in iterfiles(path):
        print(a)

    print('*** iterfiles with predicate')
    for a in iterfiles(path, lambda x: x.lower().endswith('.py')):
        print(a)

    print('*** walk')
    for a in walk(path, '*.py'):
        print(a)

    print('*** walk')
    for a in walk(path, '*.py;*.pyc'):
        print(a)

    print('*** walk')
    for a in walk(path, yield_folders=True):
        print(a)

if __name__ == '__main__':
    test(sys.argv[1])
