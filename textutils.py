#!/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import, unicode_literals

import os.path
import six
import base64

def reindent(text, numspaces):
    spaces = ' ' * numspaces
    lines = [spaces + x.strip() for x in text.splitlines()]
    return '\n'.join(lines)

def to_content(content_or_filepath):
    # return content as unicode
    try:
        if os.path.isfile(content_or_filepath):        
            with open(content_or_filepath, 'rb') as fp:
                content = fp.read()
                #print('content:', type(content), content)
                return to_text(content)
    except:
        pass

    return to_text(content_or_filepath)

def to_text(s, encoding='utf-8'):
    # convert to (Unicode) textual data. 
    # This is unicode() in Python 2 and str in Python 3.

    if isinstance(s, six.text_type):
        return s

    return six.text_type(s, encoding)

def to_binary(s, encoding='utf-8'):
    # convert to binary data. 
    # This is str in Python 2 and bytes in Python 3.
    
    if isinstance(s, six.binary_type):
        return s

    if isinstance(s, six.text_type):
        return s.encode(encoding)

def b64encode(b):
    # convert binary date to base84-encoded unicode
    # This is str in Python 2 and bytes in Python 3.
    
    if not isinstance(b, six.binary_type):
        raise RuntimeError('arg is not binary')

    return six.text_type(base64.b64encode(b), 'utf-8')

def b64decode(t):
    return base64.b64decode(t)

def test():
    content = open(__file__, 'rb').read()
    assert content == to_content(content), 'to_content failed.'
    assert content == to_content(__file__), 'to_content failed.'

    text = ('satoshi\n'
            'satoshi\n'
            'satoshi\n')

    print(reindent(text, 1))
    print(reindent(text, 2))
    print(reindent(text, 3))

if __name__ == '__main__':
    test()
