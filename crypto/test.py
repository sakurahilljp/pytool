#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import, unicode_literals

import json
import base64
import os.path
import six

from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES

def digest():
    msg = 'this is unicode'
    print('msg:', type(msg), msg)

    assert isinstance(msg, six.text_type), 'not textual'

    digest = SHA256.new(msg.encode('utf-8')).hexdigest()
    print('digest:', type(digest), digest)
    assert isinstance(digest, six.string_types), 'not binary'


def test():
    digest()
    
if __name__ == '__main__':
    test()
