#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import, unicode_literals

import six
from Crypto.Hash import SHA256

from ..textutils import to_text

def sha256(msg):
    msg = to_text(msg)
    digest = SHA256.new(msg.encode('utf-8')).hexdigest()
    return to_text(digest)
    
def test():
    digest = sha256('message')
    print('digest:', type(digest), digest)
    assert isinstance(digest, six.text_type)
