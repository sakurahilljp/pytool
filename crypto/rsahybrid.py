#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import, unicode_literals

import json
import os.path

from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
from Crypto.Cipher import AES
from Crypto import Random

import tempfile

from ..textutils import to_content, to_text, b64encode, b64decode

def encrypt(obj, pubkey):
    ''' encrypt python object'''
    rsakey = to_content(pubkey)
    
    # encrypt obj bye AES
    skey = Random.new().read(32) # 256 bits
    iv = Random.new().read(AES.block_size)
    aes = AES.new(skey, AES.MODE_CFB, iv)
    msg = iv + aes.encrypt(json.dumps(obj))
    # encrypt AES shared key by RSA public key
    rsa = PKCS1_OAEP.new(RSA.importKey(rsakey))
    key = rsa.encrypt(skey)

    payload = { 'key': b64encode(key),
                'msg': b64encode(msg) }
    return json.dumps(payload)
    
def decrypt(payload, prikey):
    ''' decrypt and return python object'''
    rsakey = to_content(prikey)

    # decode json to binary data
    payload = json.loads(payload)
    key = b64decode(payload['key'])
    msg = b64decode(payload['msg'])

    # decrypt AES shared key bye RSA
    rsa = PKCS1_OAEP.new(RSA.importKey(rsakey))
    skey = rsa.decrypt(key)

    # decript msg by RSA private key
    iv = msg[:AES.block_size]
    aes = AES.new(skey, AES.MODE_CFB, iv)
    msg = aes.decrypt(msg[AES.block_size:])
    return json.loads(to_text(msg))


def test():
    print('AES.block_size:', AES.block_size)

    rsa = RSA.generate(2048)
    private_pem = rsa.exportKey('PEM')
    public_pem = rsa.publickey().exportKey()

    rsa2 = RSA.generate(2048)
    private_pem2 = rsa2.exportKey('PEM')
    public_pem2 = rsa2.publickey().exportKey()

    print(private_pem)
    print(public_pem)
    print(private_pem2)
    print(public_pem2)
    
    prifile = tempfile.NamedTemporaryFile()
    prifile.write(private_pem)
    prifile.flush()

    pubfile = tempfile.NamedTemporaryFile()
    pubfile.write(public_pem)
    pubfile.flush()

    print('[test] with key string')
    obj = 'this is a message.'

    v = encrypt(obj, public_pem)
    obj2 = decrypt(v, private_pem)
    assert obj == obj2, 'encrypt/decrypt failed.'

    obj = None
    v = encrypt(obj, public_pem)
    obj2 = decrypt(v, private_pem)
    assert obj == obj2, 'encrypt/decrypt failed.'


    print('[test] with key file')
    print('prifile.name', prifile.name)
    print('pubfile.name', pubfile.name)
    obj = 'this is a message.'
    v = encrypt(obj, pubfile.name)
    obj2 = decrypt(v, prifile.name)
    assert obj == obj2, 'encrypt/decrypt failed.'

    obj = None
    v = encrypt(obj, pubfile.name)
    obj2 = decrypt(v, prifile.name)
    assert obj == obj2, 'encrypt/decrypt failed.'

    obj = u'日本語'
    v = encrypt(obj, pubfile.name)
    obj2 = decrypt(v, prifile.name)
    assert obj == obj2, 'encrypt/decrypt failed.'

    try:
        v = encrypt(obj, public_pem2)
        obj2 = decrypt(v, private_pem)
        assert False, 'encrypt/decrypt failed.'
    except ValueError as e:
        #print(type(e), e)
    	pass

if __name__ == '__main__':
    test()
