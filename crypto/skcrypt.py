#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import, unicode_literals

import uuid
import json

from Crypto.Cipher import AES
from Crypto import Random

from ..textutils import to_binary, to_text, b64encode, b64decode

def keygen():
    return uuid.uuid4().hex

def encrypt(obj, skey):
    ''' encrypt python object'''
    
    # encrypt obj bye AES
    skey = uuid.UUID(skey).bytes
    iv = Random.new().read(AES.block_size)
    aes = AES.new(skey, AES.MODE_CFB, iv)
    msg = iv + aes.encrypt(json.dumps(obj))

    return b64encode(msg)
    
def decrypt(payload, skey):
    ''' decrypt and return python object'''

    msg = b64decode(payload)

    # decript msg by RSA private key
    skey = uuid.UUID(skey).bytes
    iv = msg[:AES.block_size]
    aes = AES.new(skey, AES.MODE_CFB, iv)
    msg = aes.decrypt(msg[AES.block_size:])
    return json.loads(to_text(msg))

def test():
    print('AES.block_size:', AES.block_size)

    skey = keygen()
    print('skey:', skey)

    print('[test] with key string')
    obj = 'this is a message.'

    v = encrypt(obj, skey)
    print(v)
    obj2 = decrypt(v, skey)
    assert obj == obj2, 'encrypt/decrypt failed.'

    obj = None
    v = encrypt(obj, skey)
    print(v)
    obj2 = decrypt(v, skey)
    assert obj == obj2, 'encrypt/decrypt failed.'

    obj = u'日本語'
    v = encrypt(obj, skey)
    print(v)
    obj2 = decrypt(v, skey)
    assert obj == obj2, 'encrypt/decrypt failed.'
    
if __name__ == '__main__':
    test()
