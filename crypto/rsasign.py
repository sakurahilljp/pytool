#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import, unicode_literals

import json
import os.path

from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES

from ..textutils import to_content, to_binary, to_text, b64encode, b64decode

def sign(prikey, message):
    prikey = to_content(prikey)
    rsakey = RSA.importKey(prikey) 
    signer = PKCS1_v1_5.new(rsakey) 
    digest = SHA256.new(to_binary(message))
    signature = signer.sign(digest)
    return b64encode(signature)

def verify(pubkey, message, signature):
    pubkey = to_content(pubkey)
    rsakey = RSA.importKey(pubkey)
    verifier = PKCS1_v1_5.new(rsakey)
    digest = SHA256.new(to_binary(message))
    signature = b64decode(signature)
    return verifier.verify(digest, signature)

def test():
    print('AES.block_size:', AES.block_size)

    rsa = RSA.generate(2048)
    private_pem = rsa.exportKey('PEM')
    public_pem = rsa.publickey().exportKey()

    message = 'this is a message.'

    signature = sign(private_pem, message)
    print(type(signature), signature)
    authenticated = verify(public_pem, message, signature)
    
    assert authenticated, 'not authenticated'



if __name__ == '__main__':
    test()
