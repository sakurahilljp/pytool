#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import, unicode_literals

import sys
import os
import traceback

ENV = 'PYTOOL_TRACE'
traceOutput = sys.stdout
watchOutput = sys.stdout
rawOutput = sys.stdout

enabled = os.environ.get(ENV) != None

# calling 'watch(secretOfUniverse)' prints out something like:
# File "trace.py", line 57, in __testTrace
# secretOfUniverse <int> = 42
watch_format = ('File "{fileName}", line {lineNumber}, in {methodName}\n'
                '{varName} <{varType}> = {value}\n\n')


def watch(variableName):
    if not enabled:
        return

    stack = traceback.extract_stack()[-2:][0]
    actualCall = stack[3] if stack[3] is not None else "watch([unknown])"
    left = actualCall.find('(')
    right = actualCall.rfind(')')
    params = dict(varName=actualCall[left + 1:right].strip(),
                  varType=str(type(variableName))[7:-2],
                  value=repr(variableName),
                  methodName=stack[2],
                  lineNumber=stack[1],
                  fileName=stack[0])
    watchOutput.write(watch_format.format(**params))
    watchOutput.flush()

# calling 'trace("this line was executed")' prints out something like:
# File "trace.py", line 64, in ?
# this line was executed
trace_format = ('File "{fileName}", line {lineNumber}, in {methodName}\n'
                '{text}\n\n')


def trace(text=''):
    if not enabled:
        return

    stack = traceback.extract_stack()[-2:][0]
    params = dict(text=text,
                  methodName=stack[2],
                  lineNumber=stack[1],
                  fileName=stack[0])
    traceOutput.write(trace_format.format(**params))
    traceOutput.flush()

# calling 'raw("some raw text")' prints out something like:
# Just some raw text


def writeline(*args, **kwargs):
    if not enabled:
        return

    write(*args, **kwargs)
    rawOutput.write('\n')
    rawOutput.flush()


def write(*args, **kwargs):
    if not enabled:
        return

    sep = kwargs.get('sep', ' ')
    args = [str(x) for x in args]

    rawOutput.write(sep.join(args))
    rawOutput.flush()


def __testTrace():
    secretOfUniverse = 42
    watch(secretOfUniverse)
    writeline('secretOfUniverse', secretOfUniverse)


def test():
    a = 'something else'
    watch(a)

    __testTrace()

    trace('this line is executed.')

    write('write', 'write', 'write')
    writeline('writeline', 'writeline')
    writeline('writeline', 'writeline')

if __name__ == '__main__':
    test()
