#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import, unicode_literals

import math
import random

class StreamStat(object):
    def __init__(self):
        self.avg = 0.0
        self.stddev = 0.0
        self.n = 0
        self.latest = 0.0

    @property
    def sigma(self):
        return math.sqrt(self.stddev)

    def append(self, v):
        n = self.n + 1
        x = self.avg
        ss = self.stddev
        xn = ((n - 1) * x + v ) / n
        ssn = ((ss + x * x) * (n - 1) + v * v)/n - xn * xn

        self.latest = v
        self.n = n
        self.avg = xn
        self.stddev = ssn
    
    def reset(self):
        self.avg = 0.0
        self.stddev = 0.0
        self.n = 0
        self.latest = 0.0

def test():
    ss = StreamStat()
    series = [random.gauss(10, 1.5) for _ in range(500)]

    for i in range(len(series)):
        n = i + 1
        v = series[i]
        ss.append(v)

        x2 = sum(series[0:n])/n
        ss2 = sum([v*v - x2*x2 for v in series[0:n]])/n

        print(i)
        print(ss.avg, ss.stddev, ss.sigma)
        print(x2, ss2)
        assert "{0:0.8}".format(ss.avg) == "{0:0.8}".format(x2)
        assert "{0:0.8}".format(ss.stddev) == "{0:0.8}".format(ss2)
        print()
    
if __name__ == '__main__':
    test()
