#!/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import, unicode_literals
import sys
import os
import os.path

__thisdir = os.path.dirname(__file__)
__parentdir = os.path.abspath(os.path.join(__thisdir, '..', '..'))
sys.path.append(__parentdir)

from pytool.rpc import rpchandler
from pytool.rpc import rpcproxy
from pytool.rpc import rpchelper

def test():
    rpchandler.test()
    rpcproxy.test()
    rpchelper.test()

if __name__ == '__main__':
    test()
