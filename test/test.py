#!/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import, unicode_literals
import sys
import os
import os.path

__thisdir = os.path.dirname(__file__)
__parentdir = os.path.abspath(os.path.join(__thisdir, '..', '..'))
sys.path.append(__parentdir)

from pytool import textutils
from pytool import trace
from pytool import zipdir

from pytool.datamodels import ringbuffer
from pytool.datamodels import flatten

from pytool.ifttt import makerchannel
from pytool.os import dummyfcntl
from pytool.os import filelock
from pytool.os import exclusivefile
from pytool.os import filesystem

from pytool.algorithm import memalloc

from pytool.crypto import rsahybrid
from pytool.crypto import skcrypt
from pytool.crypto import digester
from pytool.crypto import rsasign
from pytool.crypto import test

from pytool.rpc import rpchandler
from pytool.rpc import rpcproxy

from pytool.inet import gmailer
from pytool.inet import mqttclient
from pytool.inet import mqtthelper
from pytool.inet import linenotify
from pytool.inet import redisclient

from pytool.statistics import streamstat

from pytool.evernote import sendever

from pytool import crypto

def crypto_test():
    crypto.rsahybrid.test()
    crypto.rsasign.test()
    crypto.digester.test()
    crypto.skcrypt.test()

if __name__ == '__main__':
    crypto_test()
