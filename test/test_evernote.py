#!/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import, unicode_literals

import sys
import os.path
import datetime
from evernote.api.client import EvernoteClient

__thisdir = os.path.dirname(__file__)
__parentdir = os.path.abspath(os.path.join(__thisdir, '..', '..'))
sys.path.append(__parentdir)

from pytool.evernote.sendever import NoteBook, SendEver

def test(token):
    
    print(repr(token))
    client = EvernoteClient(token=token, sandbox=False)

    # NoteBook class
    faxnotes = NoteBook('Sandbox', client)
    faxnotes.create_note('textual only', 'textual only', [])
    faxnotes.create_note('textual with single attachment', 'textual', ['PDF_sample.pdf'])
    faxnotes.create_note('textual with multipule attachments', 'textual', ['PDF_sample.pdf', 'unnamed.jpg'])

    # send
    attachments = [('PDF_sample.pdf', open('PDF_sample.pdf', 'rb').read()),
                   ('unnamed.jpg', open('unnamed.jpg', 'rb').read())]
    SendEver.send(client, 'Sandbox', 'title', 'attachment tuples', attachments)
    
if __name__ == '__main__':
    test(sys.argv[1])
