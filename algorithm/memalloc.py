#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import, unicode_literals

class MemoryAllocator(object):
    DEFAULT_BLOCK_SIZE = 1024 * 4
    
    def __init__(self, capacity, blocksize = 0):
        self.capacity = capacity
        self.blocksize = blocksize if blocksize > 0 else self.DEFAULT_BLOCK_SIZE
        self.maxblocks = int(capacity / self.blocksize)
        self.spaces = [[0, self.maxblocks]]

    @property
    def freeblocks(self):
        return sum([fs[1] for fs in self.spaces])

    @property
    def freespace(self):
        return self.freeblocks * self.blocksize
    
    def acquire(self, length):
        count = int((length - 1) / self.blocksize) + 1

        found = None
        for fs in self.spaces:
            if fs[1] >= count:
                found = fs
                break
        if fs is None: raise RuntimeError('not enough space')

        offset = fs[0] * self.blocksize
        
        if fs[1] == count:
            self.spaces.remove(fs)
        else:
            fs[0] += count
            fs[1] -= count

        return (offset, length)

    def release(self, chunk):
        offset = chunk[0]
        length = chunk[1]
        fs = [int(offset / self.blocksize), int((length -1) / self.blocksize) +1]

        self.spaces.append(fs)
        self.spaces.sort(key=lambda fs: fs[0])

        # concat spaces
        i = 0
        while i + 1 < len(self.spaces):
            pre = self.spaces[i]
            post = self.spaces[i+1]
            if pre[0] + pre[1] == post[0]:
                pre[1] += post[1]
                self.spaces.remove(post)
            else:
                i += 1
        
    def dump(self):
        for fs in self.spaces:
            print('index = {}, size = {}'.format(fs[0], fs[1]))
        print('free space = {} ({})'.format(self.freespace, self.freeblocks))
        print()
    
def test():
    allocator = MemoryAllocator(MemoryAllocator.DEFAULT_BLOCK_SIZE * 100)

    ofs1 = allocator.acquire(32)
    print(ofs1)
    
    allocator.dump()

    ofs2=allocator.acquire(32)
    print(ofs2)
    
    allocator.dump()

    ofs3 = allocator.acquire(1024)
    print(ofs3)
    allocator.dump()

    print('-- release {}'.format(ofs1))
    allocator.release(ofs1)
    allocator.dump()

    ofs4 = allocator.acquire(1024)
    print(ofs4)
    allocator.dump()
    
    print('-- release {}'.format(ofs2))
    allocator.release(ofs2)
    allocator.dump()

    print('-- release {}'.format(ofs3))
    allocator.release(ofs3)
    allocator.dump()

    print('-- release {}'.format(ofs4))
    allocator.release(ofs4)
    allocator.dump()
    
if __name__ == '__main__':
    test()
