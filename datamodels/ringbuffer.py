#!/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import, unicode_literals

from collections import deque


class RingBuffer(object):

    def __init__(self, size_max):
        self.size_max = size_max
        self.data = []

    def append(self, x):
        self.data.append(x)
        if len(self.data) == self.size_max:
            self.append = self._full_append
            self.tolist = self._full_tolist
            self.getitem = self._full_getitem
            self.cur = 0

    def tolist(self):
        return self.data

    def getitem(self, index):
        return self.data[index]

    def __getitem__(self, index):
        return self.getitem(index)

    def __str__(self):
        return str(self.tolist())

    def _full_append(self, x):
        self.data[self.cur] = x
        self.cur = (self.cur + 1) % self.size_max

    def _full_tolist(self):
        return self.data[self.cur:] + self.data[:self.cur]

    def _full_getitem(self, index):
        if index < 0 or index >= self.size_max:
            raise IndexError()
        return self.data[(self.cur + index) % self.size_max]


class DequeRingBuffer(deque):

    def __init__(self, size_max):
        deque.__init__(self)
        self.size_max = size_max

    def append(self, x):
        deque.append(self, x)
        if len(self) == self.size_max:
            self.append = self._full_append

    def _full_append(self, x):
        deque.append(self, x)
        self.popleft()

    def tolist(self):
        return list(self)


def test():
    rb = RingBuffer(5)
    for i in range(5):
        rb.append(i)
    print(rb)

    for x in rb:
        print(x)

    rb.append(5)
    print(rb)
    for x in rb:
        print(x)

    rb.append(6)
    print(rb)
    for x in rb:
        print(x)

    rb = DequeRingBuffer(5)
    for i in range(5):
        rb.append(i)
    print(rb)

    for x in rb:
        print(x)

    rb.append(5)
    print(rb)
    for x in rb:
        print(x)

    rb.append(6)
    print(rb)
    for x in rb:
        print(x)

if __name__ == '__main__':
    test()
