#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import, unicode_literals


def iterable(x):
    try:
        iter(x)
        return True
    except:
        return False


def flatten(sequence, to_expand=iterable):
    iterators = [iter(sequence)]
    while iterators:
        # この時点で最も深い(リストの最後の)反復子にループをかける
        for item in iterators[-1]:
            if to_expand(item):
                # サブシーケンスがある。そちらの反復子をループしに行く
                iterators.append(iter(item))
                break
            else:
                yield item
        else:
            iterators.pop()


def flatten_r(sequence, to_expand=iterable):
    ''' 再帰版 '''

    for item in sequence:
        if to_expand(item):
            for subitem in flatten(item, to_expand):
                yield subitem
        else:
            yield item


def main():
    sample = [1, 2, [3, [], 4, [5, 6], 7, [8, ], ], 9]

    print(sample)

    f1 = flatten(sample)
    f2 = flatten_r(sample)

    print(list(f1))
    print(list(f2))

    for x in zip(f1, f2):
        assert x[0] == x[1]

if __name__ == '__main__':
    main()
