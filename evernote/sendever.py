#!/bin/env python
# -*- coding: utf-8 -*-
#from __future__ import division, print_function, absolute_import, unicode_literals

#!/usr/bin/env python
# -*- coding: utf-8 -*-

from evernote.api.client import EvernoteClient, Store
import evernote.edam.notestore.NoteStore as NoteStore
from evernote.edam.type.ttypes import Note, Notebook
from evernote.edam.type.ttypes import Resource, ResourceAttributes, Data

import os
import mimetypes
import hashlib
import os.path
import datetime
import sys

class NoteBook(object):

    def __init__(self, name, client):
        self.name = name
        self.client = client
        self.guid = None

        store = self.client.get_note_store()

        for nb in store.listNotebooks():
            # Notebook.name property is not unicode.
            if nb.name == name.encode('utf-8'):
                self.guid = nb.guid

        if not self.guid:
            notebook = Notebook()
            notebook.name = name
            notebook = store.createNotebook(notebook)
            self.guid = notebook.guid
            
    def create_note(self, title, content, attachments):
        store = self.client.get_note_store()

        note = Note()
        note.notebookGuid = self.guid

        if attachments:
            resources = [NoteBook.create_resource(x) for x in attachments]
            note.resources = resources
            for resource in resources:
                content += '<span><en-media type="{}" hash="{}"/></span>'.format(resource.mime, resource.data.bodyHash)

        note.title = title
        note.content = '<?xml version="1.0" encoding="UTF-8"?>'
        note.content += '<!DOCTYPE en-note SYSTEM "http://xml.evernote.com/pub/enml2.dtd">'
        note.content += '<en-note>{}</en-note>'.format(content)
        created_note = store.createNote(note)
            
    @classmethod
    def create_resource(cls, attachment):
        if isinstance(attachment, basestring):
            filename = os.path.basename(attachment)
            body = open(attachment, 'rb').read()
        else:
            filename = attachment[0]
            body = attachment[1]

        data = Data()
        data.body = body
        data.size = len(data.body)
        data.bodyHash = hashlib.md5(data.body).hexdigest()
        resource = Resource()
        resource.mime = mimetypes.guess_type(filename)[0]
        resource.data = data
        attr = ResourceAttributes()
        attr.fileName = filename
        resource.attributes = attr
        return resource

class SendEver(object):
    
    @classmethod
    def send(cls, client, shelfname, title, content, attachments):
        shelf = NoteBook(shelfname, client)
        shelf.create_note(title, content, attachments)
