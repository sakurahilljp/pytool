#!/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import, unicode_literals

from contextlib import closing

from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header
import smtplib

class GmailSender(object):

    # 安全性の低いアプリのアクセスを有効にする必要あり
    # https://www.google.com/settings/security/lesssecureapps

    def __init__(self, account, password):
        self.account = account
        self.password = password

    def send(self, to_addr, subject, body, media=None):
        msg = self.__create_message(self.account, to_addr, subject, body, media)
        self.__send(to_addr, msg)
        
    def __send(self, to_addr, msg):
        with closing(smtplib.SMTP('smtp.gmail.com', 587)) as smtpc:
            smtpc.ehlo()
            smtpc.starttls()
            smtpc.ehlo()
            smtpc.login(self.account, self.password)
            smtpc.sendmail(self.account, [to_addr], msg.as_string())

    def __create_message(self, from_addr, to_addr, subject, body, media=None):
        msg = None
        charset = 'utf-8'
        if media:
            msg = MIMEMultipart()
            msg.attach(MIMEText(body, 'plain', charset))
            msg.attach(MIMEImage(media))
        else:
            msg = MIMEText(body, 'plain', charset)

        msg['Subject'] = Header(subject, charset)
        msg['From'] = from_addr
        msg['To'] = to_addr
        return msg

def main():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--to', default=None, required=True)
    parser.add_argument('--account', default=None, required=True)
    parser.add_argument('--password', default=None, required=True)
    args = parser.parse_args()

    gsender = GmailSender(args.account, args.password)
    
    subject = u'タイトル'
    body = u'本文'

    gsender.send(args.to, subject, body)


if __name__ == '__main__':
    main()
