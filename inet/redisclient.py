#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import, unicode_literals

import redis
import os

class RedisClient(object):

    def __init__(self, endpoint=None):
        self.endpoint = endpoint

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()
    
    def __getattr__(self, name):
        return getattr(self.client, name)

    def connect(self):
        self.client = redis.from_url(self.endpoint)

    def close(self):
        del self.client
