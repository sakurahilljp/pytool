#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import, unicode_literals


def match_topic(key, topic):
    key = key.split('/')  # includes wildcard (+/#)
    topic = topic.split('/')

    for i in range(len(topic)):
        if topic[i] == key[i]:
            continue
        elif key[i] == '+':
            continue
        elif key[i] == '#':
            return True
        else:
            return False

    return True


def test_match_topic():
    assert match_topic('sensors/#', 'sensors/1/temperature') == True
    assert match_topic('sensors/#', 'sensors/2/temperature') == True
    assert match_topic('sensors/#', 'sensors/1/humidity') == True
    assert match_topic('sensors/#', 'sensors/2/humidity') == True

    assert match_topic('sensors/#', 'sensors/1/temperature') == True
    assert match_topic('sensors/#', 'sensors/2/temperature') == True
    assert match_topic('sensors/#', 'sensors/1/humidity') == True
    assert match_topic('sensors/#', 'sensors/2/humidity') == True

    assert match_topic('sensors/+/temperature',
                       'sensors/1/temperature') == True
    assert match_topic('sensors/+/temperature',
                       'sensors/2/temperature') == True
    assert match_topic('sensors/+/temperature', 'sensors/1/humidity') == False
    assert match_topic('sensors/+/temperature', 'sensors/2/humidity') == False

    assert match_topic('+/+/temperature', 'sensors/1/temperature') == True
    assert match_topic('+/+/temperature', 'sensors/2/temperature') == True
    assert match_topic('+/+/temperature', 'sensor/1/temperature') == True
    assert match_topic('+/+/temperature', 'sensor/2/temperature') == True
    assert match_topic('+/+/temperature', 'sensors/1/humidity') == False
    assert match_topic('+/+/temperature', 'sensors/2/humidity') == False

def test():
    test_match_topic()


if __name__ == '__main__':
    test()
