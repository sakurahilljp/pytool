#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import, unicode_literals

import paho.mqtt.client as mqtt
import os
import six

from six.moves.urllib.parse import urlparse
from .mqtthelper import match_topic

class MqttClient(object):

    def __init__(self, endpoint):
        self.endpoint = endpoint
        self.callbacks = {}
        self.connected = False

    def __del__(self):
        self.disconnect()

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.disconnect()

    def connect(self):
        if self.connected:
            return

        self.client = mqtt.Client()
        self.client.on_connect = self.__on_connect
        self.client.on_disconnect = self.__on_disconnect
        self.client.on_message = self.__on_message
            
        url = urlparse(self.endpoint)
        if url.username and url.password:
            self.client.username_pw_set(url.username, url.password)

        self.client.connect(url.hostname, url.port)
        self.client.loop_start()
        self.connected = True

    def secure_connection(self):
        self.connect()

    def disconnect(self):
        if not self.connected:
            return 

        self.connected = False
        self.client.loop_stop()
        self.client.disconnect()
        del self.client

    def publish(self, topic, payload, retain=False):
        self.client.publish(topic, payload, retain)

    def subscribe(self, topic, callback):
        '''
        callback function: callback(topic, payload)
        '''
        self.callbacks[topic] = callback

    def __on_connect(self, client, userdata, flags, rc):
        self.connected = True

        for key in self.callbacks.keys():
            self.client.subscribe(key)

    def __on_disconnect(self, client, userdata, rc):
        self.connected = False

    def __on_message(self, client, userdata, msg):
        for topic in self.callbacks.keys():
            if match_topic(topic, msg.topic):
                self.callbacks[topic](msg.topic, msg.payload)
