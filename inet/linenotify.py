#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import, unicode_literals

import sys
import requests
from six.moves.urllib.parse import urljoin

class LineNotify(object):
    endpoint='https://notify-api.line.me/api/'

    def __init__(self, access_token):
        self.access_token = access_token

    def notify(self, msg, img=None):
        url = urljoin(self.endpoint, 'notify')
        headers = {'Authorization':'Bearer {}'.format(self.access_token)}
        data = {'message': msg}
        files = {}
        if img:
            files['imageFile'] = img

        r = requests.post(url, headers=headers, data=data, files=files)
        r.raise_for_status()
        #print(r.request.headers)
        #print(r.request.body)
        #print(r.headers)

if __name__ == '__main__':
    notify = LineNotify(sys.argv[1])
    notify.notify('test')
    notify.notify('with image', open('twitter.jpg','rb').read() )
