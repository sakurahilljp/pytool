#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import, unicode_literals

import sys
import os
import os.path
import zipfile


def zipdir(targetdir, outputdir='.', debug=False):

    outputdir = os.path.abspath(outputdir)
    targetdir = os.path.abspath(targetdir)
    parentdir = os.path.dirname(targetdir)
    zipfname = os.path.join(outputdir, os.path.basename(targetdir) + '.zip')

    if debug:
        print("output directory:", outputdir)
        print("target directory:", targetdir)
        print("parent directory:", parentdir)
        print("zip file name:", zipfname)

    zipf = zipfile.ZipFile(zipfname, 'w')
    try:
        for path, subdirs, files in os.walk(targetdir):
            for name in files:
                absname = os.path.join(path, name)
                arcname = absname[len(parentdir):]
                if arcname.startswith(os.path.sep):
                    arcname = arcname[1:]
                zipf.write(absname, arcname)
                if debug:
                    print(arcname)
    finally:
        zipf.close()

if __name__ == '__main__':

    outputdir = os.getcwd()
    for f in sys.argv[1:]:
        zipdir(f, outputdir, True)
