#!/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import, unicode_literals

from . import rpchelper

class RpcProxyAttribute(object):
    def __init__(self, call, funcname):
        self.__call = call
        self.__funcname = funcname

    def __getattr__(self, key):
        path = '.'.join([self.__funcname, key])
        return RpcProxyAttribute(self.__call, path)

    def __call__(self, *args, **kwargs):
        return self.__call(self.__funcname, *args, **kwargs)
        


class RpcProxy(object):
    def __init__(self, endpoint):
        if callable(endpoint):
            self.endpoint = endpoint
        else:
            self.endpoint = endpoint.ask

    def __getattr__(self, name):
        name = rpchelper.to_text(name)
        return RpcProxyAttribute(self.__call, name)

    def __getitem__(self, name):
        name = rpchelper.to_text(name)
        return self.__func(name)

    def __func(self, funcname):
        def do_rpc(*args, **kwargs):
            return self.__call(funcname, *args, **kwargs)
        return do_rpc

    def __call(self, funcname, *args, **kwargs):
        if __debug__: print('rpcproxy.__call:', repr(funcname), repr(args), repr(kwargs))

        request = rpchelper.dumps((funcname, args, kwargs))
        response = self.endpoint(request)
        result = rpchelper.loads(response)
        if isinstance(result, Exception):
            raise result
        return result
        

def test():
    from .rpchandler import RpcHandler
    handler = RpcHandler()

    @handler.remotecall()
    def add(x, y):
        return x + y

    @handler.remotecall('x.y.add')
    def x_y_add(x, y):
        return x + y

    print(handler.funcs)

    proxy = RpcProxy(handler)
    print(proxy.add(1 ,1))
    print(proxy.x.y.add(3 ,3))

    assert proxy.add(1 ,1) == 2
    assert proxy.x.y.add(3 ,3) == 6

    try:
        print(proxy.add('a' ,1))
        assert False
    except Exception as ex:
        #print(repr(ex))
        pass

    proxy = RpcProxy(handler.ask)
    assert proxy.add(1 ,1) == 2
    
if __name__ == '__main__':
    test()
