#!/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import, unicode_literals

import pickle
import six

from ..textutils import to_text, b64encode, b64decode

PROTOCOL = 2 # Compatible with Python 2.3 or later

def dumps(obj):
    x = pickle.dumps(obj, protocol=PROTOCOL)
    return b64encode(x)

def loads(x):
    x = b64decode(x)
    if six.PY2:
        return pickle.loads(x)        
    if six.PY3:
        return pickle.loads(x, encoding='bytes')
    
def test():
    o = 'test'
    print(repr(o))
    x = dumps(o)
    print(repr(x))
    y = loads(x)
    print(repr(y))
    assert o == y
    
    if six.PY3:
        o = b'test'
        print(repr(o))
        x = dumps(o)
        print(repr(x))
        y = loads(x)
        print(repr(y))
        assert o == y
    
if __name__ == '__main__':
    test()
