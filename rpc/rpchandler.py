#!/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import, unicode_literals

from . import rpchelper

class Error(Exception):
    pass

class RpcHandler(object):
    def __init__(self):
        self.funcs = {}
        self.instance = None
        self.allow_dotted_names = True

    def register_function(self, func, name=None):
        if name:
            funcname = name
        else:
            funcname = func.__name__

        if funcname in self.funcs:
            raise Error('"{}" was already registered.'.format(funcname))

        self.funcs[funcname] = func

    def register_instance(self, instance, allow_dotted_names=True):
        self.instance = instance
        self.allow_dotted_names = allow_dotted_names
    
    def remotecall(self, funcname=None):
        #decorator
        def decorator(func):
            self.register_function(func, funcname)
            return func
        return decorator

    def call(self, funcname, *args, **kwargs):
        if __debug__: print('[rpchandler.call]', repr(funcname), repr(args), repr(kwargs))

        func = None
        if self.instance:
            # invoke instance method.
            if self.allow_dotted_names:
                names = list(reversed(funcname.split('.')))
                obj = self.instance
                while len(names) > 0:
                    if hasattr(obj, names[-1]):
                        obj = getattr(obj, names[-1])
                        names.pop()
                    else:
                        break

                if len(names) == 0:
                    if not callable(obj):
                        raise RuntimeError('"{0}" is not callable'.format(funcname))
                    func = obj

            else:
                if hasattr(self.instance, funcname):
                    func = getattr(self.instance, funcname)

        if not func:
            func = self.funcs[funcname]
        return func(*args, **kwargs)

    def ask(self, request):
        if __debug__: print("[rpchandler.ask]", type(request))

        funcname, args, kwargs = rpchelper.loads(request)

        try:
            r = self.call(funcname, *args, **kwargs)
            return rpchelper.dumps(r)
        except Exception as e:
            if __debug__:
                print('[rpchandler.ask] call raise exception:',repr(e))
            return rpchelper.dumps(e)

def test():
    handler = RpcHandler()

    def add(x, y):
        return x + y

    handler.register_function(add)

    @handler.remotecall()
    def sub(x, y):
        return x - y


    @handler.remotecall('calc.add')
    def calc_add(x, y):
        return x + y

    try:
        @handler.remotecall()
        def add(x, y):
            return x + y

        assert False, 'Error should occur!'
    except Error as ex:
        #print(repr(ex))
        pass
    
    class SubService(object):
        def subhello(self, name):
            return 'sub hello {}'.format(name)

    class Service(object):

        def __init__(self):
            self.subsvc = SubService()

        def hello(self, name):
            return 'hello {}'.format(name)

    svc = Service()
    handler.register_instance(svc)
        
    print(handler.funcs)

    assert handler.funcs['add'](1,1) == 2
    assert handler.funcs['sub'](1,1) == 0    
    assert handler.funcs['calc.add'](1,2) == 3

    handler.call('hello', 'you')
    handler.call('subsvc.subhello', 'you')

    try:
        handler.call('subsvc', 'you')
        assert False
    except:
        pass

if __name__ == '__main__':
    test()
