#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import, unicode_literals

import sys
import requests

class MakerChannel(object):

    def __init__(self, key):
        self.key = key
        self.urltemplate = 'https://maker.ifttt.com/trigger/{}/with/key/{}'

    def trigger(self, event, args=None):
        url = self.urltemplate.format(event, self.key)
        if args is None:
            r = requests.get(url)
        else:
            r = requests.post(url, json=args)

        # print(r.url)

def test(key):
    mc = MakerChannel(key)
    mc.trigger('test')
    mc.trigger('test', {'value1':'OK', 'value2':'POST', 'value3':'PASS'})

if __name__ == '__main__':
    test(sys.argv[1])
    
